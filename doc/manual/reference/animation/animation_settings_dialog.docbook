<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="animation.animation_settings_dialog"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Animation settings</title>

    <para>
     <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/animation_settings_dialog/animation_settings_dialog.png" format="PNG" scale="60" />
     </imageobject></mediaobject></screenshot></informalfigure>

      This dialog box can be opened from the animation toolbar:
      <screenshot><mediaobject><imageobject>
         <imagedata fileref="images/animation_settings_dialog/animation_settings_button.png" format="PNG" scale="38" />
      </imageobject></mediaobject></screenshot>
      It allows you to change animation-related settings, e.g. the playback frame rate and the length of the
      current animation.
   </para>
   <para>
      Further information on OVITO's animation system can be found <link linkend="usage.animation">elsewhere</link>.
   </para>

  <simplesect>
    <title>Settings</title>

    <variablelist>
       <varlistentry>
        <term>Frames per second</term>
        <listitem><para>
           Sets the playback speed of the animation in frames per second. In particular, this determines the frame rate of
           video files rendered by OVITO.
		    </para></listitem>
      </varlistentry>

      <varlistentry>
        <term>Animation start / end</term>
        <listitem><para>
           Specifies the length of the animation interval shown in
           the timeline in OVITO's main window. Normally, this interval is automatically determined
           by the length of the loaded simulation sequence. But these controls allow you to
           override the length if needed. This is useful, for example, if the loaded simulation
           dataset consists of a single static frame only and you want to create a <link linkend="usage.animation">camera animation</link>
           with a user-defined length.
		      </para></listitem>
      </varlistentry>

      <varlistentry>
        <term>Playback speed in viewports</term>
        <listitem><para>
           Controls the playback speed of the animation in OVITO's interactive viewports.
           This factor is multiplied with the frame rate
           parameter above. Note that the effective playback speed may be much slower if
           loading, computing, and displaying the next frame takes too long.
		    </para></listitem>
      </varlistentry>

      <varlistentry>
        <term>Loop playback</term>
        <listitem><para>
           Controls whether animation playback in the viewport happens in a repeating loop.
           If turned off, playback stops when the end of the animation is reached.
		    </para></listitem>
      </varlistentry>

    </variablelist>
  </simplesect>

</section>
