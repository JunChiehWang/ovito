test_flag = False

# >>>>>>>>> snippet start here >>>>>>>>>>>>>>>
import matplotlib
matplotlib.use('Agg') # Activate 'agg' backend for off-screen plotting.
import matplotlib.pyplot as plt
import PyQt5.QtGui

def render(args):
    # Request the output data collection from the current pipeline:
    data = args.scene.selected_pipeline.compute()
    # Look up the DataSeries object generated by the CoordinationAnalysisModifier:
    if 'coordination-rdf' not in data.series: 
        raise RuntimeError('No RDF data found')
    rdf_data = data.series['coordination-rdf'].as_table()

    #  Compute plot size in inches (DPI determines label size)
    dpi = 80
    plot_width = 0.5 * args.size[0] / dpi
    plot_height = 0.5 * args.size[1] / dpi
    
    # Create matplotlib figure:
    fig, ax = plt.subplots(figsize=(plot_width,plot_height), dpi=dpi)
    fig.patch.set_alpha(0.5)
    plt.title('Coordination')
    
    # Plot RDF histogram data
    ax.bar(rdf_data[:,0], rdf_data[:,1])
    plt.tight_layout()
    
    # Render figure to an in-memory buffer.
    buf = fig.canvas.print_to_buffer()
    
    # Create a QImage from the memory buffer
    res_x, res_y = buf[1]
    img = PyQt5.QtGui.QImage(buf[0], res_x, res_y, PyQt5.QtGui.QImage.Format_RGBA8888)
    
    # Paint QImage onto viewport canvas 
    args.painter.drawImage(0, 0, img)	


# <<<<<<<<<<<<< snippet ends here <<<<<<<<<<<<<
    global test_flag
    test_flag = True # Indicate to the driver script that this function has been executed

from ovito.vis import PythonViewportOverlay, Viewport, TachyonRenderer
from ovito.io import import_file
from ovito.modifiers import CoordinationAnalysisModifier

pipeline = import_file("input/simulation.dump")
pipeline.add_to_scene()
pipeline.modifiers.append(CoordinationAnalysisModifier(cutoff = 4.0))
viewport = Viewport(type = Viewport.Type.Top)
viewport.overlays.append(PythonViewportOverlay(function = render))
viewport.render_image(filename='output/simulation.png', 
                size=(200, 200), 
                renderer=TachyonRenderer())
assert(test_flag)