.. _example_scale_bar_overlay:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Example O1: Scale bar
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following script renders a scale bar into the viewport (with a fixed length of 4 nm, as shown in the example picture).
You can copy/paste the source code into the script input field and adjust the parameters in the code as needed.

.. image:: /../manual/images/viewport_overlays/python_script_scale_bar_example.png

.. literalinclude:: ../../../example_snippets/overlay_scale_bar.py
  :lines: 4-35