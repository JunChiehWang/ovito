from ovito.io import import_file
from ovito.modifiers import PolyhedralTemplateMatchingModifier
import numpy as np

# Temporarily disabling this test, because the Python binding for 
# PolyhedralTemplateMatchingModifier.Algorithm has been removed from this development branch.
import sys
sys.exit()

pipeline = import_file("../files/CFG/fcc_coherent_twin.0.cfg")
data = pipeline.source.data

algo = PolyhedralTemplateMatchingModifier.Algorithm(data,
    { PolyhedralTemplateMatchingModifier.Type.FCC,
      PolyhedralTemplateMatchingModifier.Type.HCP })

for i in range(data.particles.count):
    stype = algo.identify(i)
    assert(stype == algo.structure)
    print("Atom {}:".format(i))
    print("  type: {}".format(stype))
    print("  rmsd: {}".format(algo.rmsd))
    print("  interatomic distance: {}".format(algo.interatomic_distance))
    print("  orientation: {}".format(algo.orientation))
    print("  def gradient: {}".format(algo.defgrad))
    print("  chemical ordering: {}".format(algo.ordering))
    print("  neighbor count: {}".format(algo.neighbor_count))
    for j in range(algo.neighbor_count):
        print("    Neighbor {}: index={} delta={} ideal={}".format(j, algo.get_neighbor(j).index, algo.get_neighbor(j).delta, algo.get_ideal_vector(j)))

        # Verify neighbor mapping generated by the PTM algorithm.
        #
        # Note: The input structure loaded above is free of elastic strains and contains only undistorted neighbor vectors.
        # Thus, the ideal template vectors must match the actual neighbor vectors exactly after applying
        # the computed lattice spacing and rotation.
        v = algo.interatomic_distance * np.dot(algo.orientation_mat, algo.get_ideal_vector(j))

        assert(np.allclose(v, algo.get_neighbor(j).delta, atol=1e-5))
